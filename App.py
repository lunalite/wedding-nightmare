import logging

from challenge import app

logger = logging.getLogger(__name__)

logFormatter = logging.Formatter(
    "%(asctime)s [%(filename)s] [%(funcName)s] [%(lineno)d] [%(levelname)-5.5s]  %(message)s")
rootLogger = logging.getLogger()

rootLogger.setLevel(logging.INFO)

fileHandler = logging.FileHandler("codeitsuisse2019PythonChallengeTemplate.log")
fileHandler.setFormatter(logFormatter)
rootLogger.addHandler(fileHandler)

consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
rootLogger.addHandler(consoleHandler)

logger.info("Starting application server....")

if __name__ == "__main__":
    app.run(port=5000)

