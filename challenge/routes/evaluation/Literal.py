class Literal:
    def __init__(self, guest, table, is_negation=False):
        self.symbol = (guest, table)
        self.is_negation = is_negation

    def eval_literal(self, val):
        if self.is_negation:
            return not val
        else:
            return val
