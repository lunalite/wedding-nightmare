from challenge.routes.evaluation.Clause import Clause
from challenge.routes.evaluation.Literal import Literal


class Logic:
    def __init__(self, tables, guests):
        self.tables = tables
        self.guests = guests

    @staticmethod
    def sit_together(indiv1, indiv2, table):
        result = set()
        for i in range(1, table + 1):
            clause_left, clause_right = Clause(), Clause()
            literal_left = Literal(indiv1, i, True)
            literal_right = Literal(indiv2, i, False)
            clause_left.add_literal(literal_left)
            clause_left.add_literal(literal_right)

            literal_left = Literal(indiv2, i, True)
            literal_right = Literal(indiv1, i, False)
            clause_right.add_literal(literal_left)
            clause_right.add_literal(literal_right)

            result.add(clause_left)
            result.add(clause_right)
        return result

    @staticmethod
    def displaced(indiv1, indiv2, table):
        result = set()
        for i in range(1, table + 1):
            clause = Clause()
            literal_left = Literal(indiv1, i, True)
            clause.add_literal(literal_left)
            literal_right = Literal(indiv2, i, True)
            clause.add_literal(literal_right)
            result.add(clause)

        return result

    def generate_family_friend_clauses(self, relationship_mat):
        family_friends_pair = set()
        for indiv1, indiv2 in relationship_mat:
            family_friends_pair.update(self.sit_together(indiv1, indiv2, self.tables))
        return family_friends_pair

    def generate_enemies_clauses(self, relationship_mat):
        enemies_pair = set()
        for indiv1, indiv2 in relationship_mat:
            enemies_pair.update(self.displaced(indiv1, indiv2, self.tables))
        return enemies_pair

    def generate_table(self):
        result = set()

        for i in range(1, self.guests + 1):
            clause = Clause()
            for j in range(1, self.tables + 1):
                literal = Literal(i, j)
                clause.add_literal(literal)
            result.add(clause)

        for a in range(1, self.guests + 1):
            for b in range(1, self.tables + 1):
                for c in range(b + 1, self.tables + 1):
                    clause = Clause()
                    literal_left = Literal(a, b, True)
                    clause.add_literal(literal_left)
                    literal_right = Literal(a, c, True)
                    clause.add_literal(literal_right)
                    result.add(clause)
        return result
