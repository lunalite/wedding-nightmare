from challenge.routes.evaluation.Model import Model


class DPLL:
    def __init__(self, kb):
        self.clauses = kb
        self.model = Model()

    def dpll_satisfiable(self):
        symbols = self.get_proposition_symbols(self.clauses)
        return self.dpll(self.clauses, symbols, self.model)

    def dpll(self, clauses, symbols, model):
        unknown_clauses = []
        for c in clauses:
            val = model.determine_value(c)
            if val is False:
                return False
            if val is None:
                unknown_clauses.append(c)

        if not unknown_clauses:
            return True

        p, value = self.find_pure_symbol(symbols, unknown_clauses)

        if p:
            return self.dpll(clauses, minus(symbols, p), model.union(p, value))
        p, value = self.find_unit_clause(clauses, model)

        if p:
            return self.dpll(clauses, minus(symbols, p), model.union(p, value))
        p, rest = symbols[0], symbols[1:]

        return (self.dpll(clauses, rest, model.union(p, True))
                or self.dpll(clauses, rest, model.union(p, False)))

    @staticmethod
    def get_proposition_symbols(clauses):
        result = set()
        for clause in clauses:
            for l in clause.literals:
                result.add(l.symbol)
        return list(result)

    @staticmethod
    def find_pure_symbol(symbols, clauses):
        for s in symbols:
            found_positive = False
            found_negative = False

            for c in clauses:
                if not found_positive and s in c.cached_positive_symbols:
                    found_positive = True
                if not found_negative and s in c.cached_negative_symbols:
                    found_negative = True
            if found_positive != found_negative:
                return s, found_positive
        return None, None

    @staticmethod
    def find_unit_clause(clauses, model):
        def unit_clauses_assign(clause, inner_model):
            p, value = None, None
            for l in clause.literals:
                symbol, positive = l.symbol, not l.is_negation
                if symbol in inner_model.assignments:
                    if inner_model.assignments[symbol] == positive:
                        return None, None
                elif p:
                    return None, None
                else:
                    p, value = symbol, positive
            return p, value

        for c in clauses:
            p, value = unit_clauses_assign(c, model)
            if p:
                return p, value
        return None, None


def minus(symbols, p):
    return [x for x in symbols if x != p]
