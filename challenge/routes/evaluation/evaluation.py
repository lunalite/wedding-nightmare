import json
from datetime import datetime

from challenge.routes.TestData import TestData
from challenge.routes.evaluation.DPLL import DPLL
from challenge.routes.evaluation.Logic import Logic


def execute_own_solution(test_data, expected_result):
    for datum in test_data:
        test_case = datum['test_case']
        tables = datum['tables']
        guests = datum['guests']
        friends = datum['friends']
        enemies = datum['enemies']
        families = datum['families']

        logic_info = Logic(tables, guests)

        tables_seats = logic_info.generate_table()
        enemies = logic_info.generate_enemies_clauses(enemies)
        families = logic_info.generate_family_friend_clauses(families)
        friends = logic_info.generate_family_friend_clauses(friends)

        information = set()
        information.update(tables_seats)
        information.update(enemies)
        information.update(families)
        information.update(friends)

        dpll = DPLL(information)
        dpll_satisfiable = dpll.dpll_satisfiable()

        expected_result.append({
            'test_case': test_case,
            'satisfiable': dpll_satisfiable
        })

    return expected_result


if __name__ == '__main__':
    startTime = datetime.now()

    with open('./input/input.json') as json_file:
        data = json.load(json_file)

    testData = TestData().generate()
    res = execute_own_solution(testData, [])

    print(res)

    print(datetime.now() - startTime)
