class Model:
    def __init__(self):
        self.assignments = {}

    def union(self, symbol, b):
        self.assignments[symbol] = b
        return self

    def determine_value(self, c):
        result = None
        if c.is_tautology():
            result = True
        elif c.is_false():
            result = False
        else:
            unassigned_symbols = False
            for positive in c.cached_positive_symbols:
                value = None
                if positive in self.assignments:
                    value = self.assignments[positive]
                if value is not None:
                    if value is True:
                        result = True
                        break
                else:
                    unassigned_symbols = True
            if result is None:
                for negative in c.cached_negative_symbols:
                    value = None
                    if negative in self.assignments:
                        value = self.assignments[negative]
                    if value is not None:
                        if value is False:
                            result = True
                            break
                    else:
                        unassigned_symbols = True
                if result is None:
                    if not unassigned_symbols:
                        result = False
        return result
