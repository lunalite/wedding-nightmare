import math
import random

from challenge.routes.model.TestCase import TestCase


class TestData:
    def __init__(self):
        self.payload = []
        self.generator = TestCase()

    def generate(self):
        # Add in all edge cases
        # tables, guests, friends, enemies, families
        self.payload += self.generator.generate(3, 15, [[1, 2], [1, 3], [1, 4], [1, 5]], [[1, 2]],
                                                [[3, 4], [6, 8], [10, 11]])  # False
        self.payload += self.generator.generate(3, 15,
                                                [[1, 1], [2, 2], [3, 3], [4, 4], [5, 5], [6, 6], [7, 7], [8, 8], [9, 9],
                                                 [10, 10], [11, 11], [12, 12]],
                                                [[1, 4], [1, 5], [2, 6], [7, 10], [7, 4], [7, 11]], [])  # True
        self.payload += self.generator.generate(3, 15, [], [[1, 2], [3, 4], [5, 6], [7, 10], [7, 4], [7, 11], [13, 13]],
                                                [])  # False
        self.payload += self.generator.generate(3, 15, [[1, 2], [4, 6], [3, 2], [2, 4], [6, 7], [3, 4], [9, 15]],
                                                [[1, 15]],
                                                [[1, 2], [1, 3], [1, 4], [1, 5], [1, 6], [6, 7], [7, 8], [8, 9],
                                                 [10, 11], [11, 12], [13, 14], [14, 15]])
        self.payload += self.generator.generate(15, 15, [[1, 2], [4, 6], [3, 2], [2, 4], [6, 7], [3, 4], [9, 15]],
                                                [[1, 12]],
                                                [[1, 2], [1, 3], [1, 4], [1, 5], [1, 6], [6, 7], [7, 8], [8, 9],
                                                 [10, 11], [11, 12], [13, 14], [14, 15]])

        # Add in random cases with increasing quantity
        for i in range(1, 95):
            tables = 1 + math.ceil(i / 20)
            guests = 15 + math.ceil(i / 5)
            factor = i / 100

            friends_alloc = []
            enemies_alloc = []
            families_alloc = []
            for guest_x in range(1, guests):
                for guest_y in range(guest_x + 1, guests):
                    rand = random.uniform(0, 1)
                    if rand <= 0.05 * factor:
                        friends_alloc.append([guest_x, guest_y])
                    elif rand <= 0.1 * factor:
                        families_alloc.append([guest_x, guest_y])
                    elif rand <= 0.15 * factor:
                        enemies_alloc.append([guest_x, guest_y])
            self.payload += self.generator.generate(
                tables=tables,
                guests=guests,
                friends=friends_alloc,
                enemies=enemies_alloc,
                families=families_alloc
            )
        random.shuffle(self.payload)
        return self.payload


if __name__ == '__main__':
    x = TestData().generate()
    print(x)

"""
15,13,16,3
6,5
5,17

16,3
2,4
2,5

5,14
"""
