import json
import logging
from threading import Thread

import requests
from flask import request, abort

from challenge import app
from challenge.routes.ResultHandler import evaluate_solution
from challenge.routes.TestData import TestData
from challenge.routes.evaluation.evaluation import execute_own_solution

logger = logging.getLogger(__name__)


@app.route('/test')
def test_route():
    return "It Works!"


@app.route('/evaluate', methods=['POST'])
def evaluate():
    logger.info("request started")

    data = request.get_json()
    try:
        team_url = data.get("teamUrl")
        callback_url = data.get("callbackUrl")
        run_id = data.get("runId")
    except AttributeError:
        logger.error('Unable to get data from post request. Check team_url, callback_url, and run_id')
        return abort(400)

    logger.info('teamUrl: %s' % team_url)
    logger.info('callbackUrl: %s' % callback_url)
    logger.info('runId: %s' % run_id)

    test_data = TestData().generate()

    result = []
    expected_result = []

    team_solution_process = Thread(target=execute_team_solution, args=[test_data, team_url, result])
    team_solution_process.start()
    official_solution_process = Thread(target=execute_own_solution, args=[test_data, expected_result])
    official_solution_process.start()

    for process in [team_solution_process, official_solution_process]:
        process.join()

    try:
        return_message = tabulate_score(result, expected_result, test_data, run_id)
    except Exception as e:
        logger.exception('Something wrong happened with tabulating score')
        return_message = {
            'runId': run_id,
            'score': 0,
            'message': e.args
        }

    r = post_callback_with_return_message(return_message, callback_url)

    logger.info('Results of posting request to %s: Status: %s' % (callback_url, r.status_code))
    logger.info('Content result: %s' % r.content)
    return 'Success'


def post_callback_with_return_message(return_message, secure_url):
    auth_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9' \
                 '.eyJzdWIiOiJjb2RlaXRzdWlzc2VhcGFjMjAxOUBnbWFpbC5jb20iLCJleHAiOjE1Njk4NjA2NTF9' \
                 '.l9PrR9r9XFA0gdqvtW1hfOm4bmHSvAVW6es1eV72v3MwjGxBCQPNbE3QtF0WtFKaLEqqaumS8Ut_KkOgG1gWCA '
    header = {'Authorization': 'Bearer ' + auth_token}
    r = requests.post(secure_url, json=return_message, headers=header)
    return r


def execute_team_solution(test_data, team_url, result):
    url = team_url + '/wedding-nightmare'
    response = requests.post(url, json=test_data)
    logger.info('teamUrl: %s' % team_url)
    logger.info('team response status code: %s' % response.status_code)
    result += json.loads(response.content.decode('utf8'))
    return True


def tabulate_score(result, expected_result, test_data, run_id):
    marks_scored, message = evaluate_solution(result, expected_result, test_data)
    response_message = {
        'runId': run_id,
        'score': marks_scored,
        'message': message
    }
    return response_message
