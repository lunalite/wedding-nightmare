class TestCase:
    test_case = 0

    def generate(self, tables, guests, friends, enemies, families):
        self.test_case += 1
        return [{
            'test_case': self.test_case,
            'tables': tables,
            'guests': guests,
            'friends': friends,
            'enemies': enemies,
            'families': families
        }]
