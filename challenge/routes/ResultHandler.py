import logging
from threading import Thread

logger = logging.getLogger(__name__)


def evaluate_solution(team_input, expected_result, test_data):
    if len(team_input) != len(test_data):
        raise IndexError(
            'Number of answers (%d) is not equal to number of questions (%d)' % (len(team_input), len(test_data)))

    number_of_correct = 0
    total_qn = len(test_data)

    for idx, team_answer in enumerate(team_input):
        if 'test_case' not in team_answer:
            raise AttributeError('test_case attribute not found in input.')
        elif 'satisfiable' not in team_answer:
            raise AttributeError('satisfiable attribute not found in input.')
        elif 'allocation' not in team_answer:
            raise AttributeError('allocation attribute not found in input.')
        if team_answer['satisfiable'] != expected_result[idx]['satisfiable']:
            continue

        if not expected_result[idx]['satisfiable']:
            number_of_correct += 1
            continue

        if check_sorted_allocation(team_answer['allocation'], test_data[idx]):
            number_of_correct += 1

    final_score = (number_of_correct / total_qn) * 100

    return final_score, "Your number_of_correct is " + str(final_score)


def check_sorted_allocation(allocation, test_datum):
    allocation.sort()

    try:
        guest_allocated = set(guest for guest, table in allocation)
        right_number_of_guests_allocated = len(guest_allocated) == test_datum['guests']
        first_guest_is_one = allocation[0][0] == 1
        last_guest_is_last = allocation[-1][0] == test_datum['guests']

        if right_number_of_guests_allocated and first_guest_is_one and last_guest_is_last:

            def check_seated_together(test, solution, result):
                for pair in test:
                    item_one, item_two = pair
                    if not solution[item_one - 1][1] == solution[item_two - 1][1]:
                        result.append(False)
                        return
                result.append(True)

            def check_not_seated_together(test, solution, result):
                for pair in test:
                    item_one, item_two = pair
                    if solution[item_one - 1][1] == solution[item_two - 1][1]:
                        result.append(False)
                        return
                result.append(True)

            cond_res = []
            friends_check_process = Thread(target=check_seated_together,
                                           args=[test_datum['friends'], allocation, cond_res])
            friends_check_process.start()
            enemies_check_process = Thread(target=check_not_seated_together,
                                           args=[test_datum['enemies'], allocation, cond_res])
            enemies_check_process.start()
            families_check_process = Thread(target=check_seated_together,
                                            args=[test_datum['families'], allocation, cond_res])
            families_check_process.start()

            for process in [friends_check_process, enemies_check_process, families_check_process]:
                process.join()

            return all(cond_res)
        else:
            return False
    except ValueError:
        raise ValueError('Please ensure allocation is passed correctly.')
