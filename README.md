# Wedding-Nightmare

As a couple that is about to get wed, both of you have decided on writing an algorithm to arrange the seats of the guests.
Due to the nature of society, most of the guests that are coming have a certain degree of familiarity with one another.
The guests could possibly be coming as friends or families. In some rare cases, there might be enemies between the guests too.

Hence, it is up to you to determine if it is possible for the guests to be seated in an arrangement that satisfies everyone.   

## Task objective
Provide an endpoint ```/wedding-nightmare```.

#### Constraints
- 15 ≤ *guests* ≤ 34
- 2 ≤ *tables* ≤ 6

#### Input format
The input given is in the form of a json array.

Each element in the array contains an object with six different parameters:
1. Test case id `test_case (integer)`
2. Number of guests: `guests (integer)`
3. Number of tables: `tables (integer)`
4. Guests who are friendly with one another and must sit in the same group: `friends (list of lists)`
5. Guests who are enemies with one another and must not sit in the same group: `enemies (list of lists)`
6. Guests that come as families and must sit in the same group: `families (list of lists)`

For points 4, 5 and 6: The given input is in the form of a tuple demarcating guests that have an attribute marked.
i.e. `"friends" : [[1, 2]]` represents that guest `1` and guest `2` are friendly.

Tables are not restricted to the number of guests.

#### Sample Input
```json
[{
  "test_case": 1,
  "guests": 4,
  "tables": 2,
  "friends": [[2, 4]],
  "enemies": [],
  "families": [],
}, {
  "test_case": 2,
  "guests": 4,
  "tables": 2,
  "friends": [[2, 4]],
  "enemies": [[2, 3], [1, 2]],
  "families": [],
}]
```
#### Explanation
Test case `1`: 
* Guest `2` &  `4` are friends.

Test case `2`:
* Guest `2` &  `4` are friends.
* Guest `2` &  `3` are enemies.
* Guest `1` &  `2` are enemies.

#### Output Format
An object with three attributes will be returned:
1. Test case id: `test_case (integer)`
2. If the allocation is satisfiable or not: `satisfiable (boolean)`
3. The allocation if it is satisfiable: `allocation (list of lists)`

#### Sample Output

```json
[
  {
    "test_case": 1,
    "satisfiable": true,
    "allocation": [[1,1], [2,1], [3,1], [4,1]]
  }, {
    "test_case": 2,
    "satisfiable": false,
    "allocation": []
  }
]
```

#### Explanation
Test case `1`:

* Problem is satisfiable and here is an example of the allocation: 

    | Guest | Table |
    | ------|:-----:| 
    | 1     | 1     |
    | 2     | 1     | 
    | 3     | 1     | 
    | 4     | 1     | 

Test case `2`: 

* Problem is not satisfiable.

Note: Tables are not limited to the number of guests.
